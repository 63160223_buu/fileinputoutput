/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.fileiputoutput;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author focus
 */
public class Computer implements Serializable{
    private int hand ; //hand: 0:rock, 1:scissors, 2:paper
    private int playerHand; //playerHand: 0:rock, 1:scissors, 2:paper
    private int win, lose, draw;
    private int status;

    public Computer() {
    }
    
    // สร้าง Method การ Random
    private int choob(){
        return ThreadLocalRandom.current().nextInt(0, 3);
    }
    
    public int poaYingChoob(int playerHand){ //win: 1, draw: 0, lose: -1
        this.playerHand = playerHand;
        this.hand = choob();
        
        if(this.playerHand == this.hand){
            draw ++;
            status = 0;
            return 0;
        }if(this.playerHand ==0 && this.hand == 1){
            win ++;
            status = 1;
            return 1;
        }if(this.playerHand ==1 && this.hand == 2){
            win ++;
            status = 1;
            return 1;
        }if(this.playerHand ==2 && this.hand == 0){
            win ++;
            status = 1;
            return 1;
        }
        status = -1;
        lose ++;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
    
    
    
}
